import { RolesGuard } from 'src/auth/guards/roles.guard';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CreateContestanPayment } from './dto/create-contestanPayment.dto';
import { CreateEventPaymentDto } from './dto/create-eventPayment.dto';
import { TransactionsService } from './transactions.service';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Roles } from 'src/users/decorators/role.decorator';
import { Role } from 'src/users/entities/role.enum';

@Controller('transactions')
@UseInterceptors(ClassSerializerInterceptor)
export class TransactionsController {
  constructor(private transactionService: TransactionsService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Post('event-payment/:event')
  async createEventPayment(
    @Param('event') event: string,
    @Body() createEventPaymentDto: CreateEventPaymentDto,
  ) {
    return {
      data: await this.transactionService.createEventPayment(
        event,
        createEventPaymentDto,
      ),
      statusCode: HttpStatus.CREATED,
      message: 'payment is successful, please wait for admin confirmation',
    };
  }

  @Get('event-payment')
  async findEventPayment() {
    const [data, count] = await this.transactionService.findEventPayment();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Post('contestan-payment/:contestan')
  async createContestanPayment(
    @Param('contestan') contestan: string,
    @Body() createContestanPayment: CreateContestanPayment,
    @Request() req,
  ) {
    return {
      data: await this.transactionService.createContestanPayment(
        contestan,
        createContestanPayment,
      ),
      statusCode: HttpStatus.CREATED,
      message:
        'payment is successful, thank you for participating in the contest',
    };
  }

  @Get('contestan-payment')
  async findContestanPayment() {
    const [data, count] = await this.transactionService.findContestanPayment();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-payment/detail/:id')
  async findOneEventPayment(@Param('id') id: string) {
    return {
      data: await this.transactionService.findOneEventPayment(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('contestan-payment/detail/:id')
  async findOneContestanPayment(@Param('id') id: string) {
    return {
      data: await this.transactionService.findOneContestanPayment(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
