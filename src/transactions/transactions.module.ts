import { MailModule } from 'src/mail/mail.module';
import { ContestansModule } from './../contestans/contestans.module';
import { EventsModule } from './../events/events.module';
import { EventPayment } from './entities/event-payment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { ContestanPayment } from './entities/contestan-payment.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([EventPayment, ContestanPayment]),
    EventsModule,
    ContestansModule,
    MailModule,
  ],
  providers: [TransactionsService],
  controllers: [TransactionsController],
})
export class TransactionsModule {}
