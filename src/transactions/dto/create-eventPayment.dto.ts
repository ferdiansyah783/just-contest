import { IsNotEmpty, IsString } from 'class-validator';

export class CreateEventPaymentDto {
  username: string;

  eventTitle: string;

  totalPayment: number;

  eventId: string;

  @IsNotEmpty()
  @IsString()
  proofOfPayment: string;
}
