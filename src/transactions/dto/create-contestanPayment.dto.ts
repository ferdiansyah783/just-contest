import { IsNotEmpty, IsString } from 'class-validator';

export class CreateContestanPayment {
  username: string;

  event: string;

  competitionPrice: number;

  ppn: number;

  totalPrice: number;

  contestanAccount: number;

  @IsNotEmpty()
  @IsString()
  proofOfPayment: string;

  contestanId: string;
}
