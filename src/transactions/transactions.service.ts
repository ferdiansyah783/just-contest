import { MailService } from 'src/mail/mail.service';
import { ContestanPayment } from './entities/contestan-payment.entity';
import { ContestansService } from './../contestans/contestans.service';
import { CreateContestanPayment } from './dto/create-contestanPayment.dto';
import { EventsService } from './../events/events.service';
import { CreateEventPaymentDto } from './dto/create-eventPayment.dto';
import { EventPayment } from './entities/event-payment.entity';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository } from 'typeorm';
import { contestPpn, profitContest } from 'src/events/helper/contest-ppn';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(EventPayment)
    private eventPaymentRepository: Repository<EventPayment>,
    @InjectRepository(ContestanPayment)
    private contestanPaymentRepository: Repository<ContestanPayment>,
    private readonly eventService: EventsService,
    private readonly contestanService: ContestansService,
    private readonly mailService: MailService,
  ) {}

  async createEventPayment(
    event: string,
    createEventPaymentRepository: CreateEventPaymentDto,
  ) {
    const existEvent = await this.eventService.findOne(event);

    const eventPayment = new EventPayment();

    eventPayment.proofOfPayment = createEventPaymentRepository.proofOfPayment;
    eventPayment.username = existEvent.user.username;
    eventPayment.eventTitle = existEvent.title;
    eventPayment.totalPayment = existEvent.competitionPrice;
    eventPayment.status = existEvent.status;
    eventPayment.eventId = existEvent.id;

    return await this.eventPaymentRepository.save(eventPayment);
  }

  async findEventPayment() {
    return await this.eventPaymentRepository.findAndCount();
  }

  async createContestanPayment(
    contestan: string,
    createContestanPayment: CreateContestanPayment,
  ) {
    const existContestan = await this.contestanService.findOne(contestan);
    const price = contestPpn(existContestan.event.competitionPrice);
    const ppn = profitContest(existContestan.event.competitionPrice);

    const contestanPayment = new ContestanPayment();

    contestanPayment.username = existContestan.user.username;
    contestanPayment.event = existContestan.event.title;
    contestanPayment.competitionPrice = existContestan.event.competitionPrice;
    contestanPayment.ppn = ppn;
    contestanPayment.totalPrice = price;
    contestanPayment.contestanAccount = existContestan.user.accountNumber;
    contestanPayment.proofOfPayment = createContestanPayment.proofOfPayment;
    contestanPayment.contestanId = existContestan.id;

    console.log(contestanPayment.proofOfPayment.slice(18));

    await this.mailService.sendImg(
      existContestan.user.email,
      contestanPayment.proofOfPayment.slice(18),
    );

    console.log(contestanPayment.proofOfPayment);

    return await this.contestanPaymentRepository.save(contestanPayment);
  }

  async findContestanPayment() {
    return await this.contestanPaymentRepository.findAndCount();
  }

  async findOneEventPayment(id: string) {
    try {
      return await this.eventPaymentRepository.findOneOrFail({
        where: {
          eventId: id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findOneContestanPayment(id: string) {
    try {
      return await this.contestanPaymentRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }
}
