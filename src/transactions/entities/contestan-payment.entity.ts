import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class ContestanPayment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;

  @Column()
  event: string;

  @Column('decimal')
  competitionPrice: number;

  @Column('decimal')
  ppn: number;

  @Column('decimal')
  totalPrice: number;

  @Column()
  proofOfPayment: string;

  @Column()
  contestanAccount: string;

  @Column()
  contestanId: string;

  @CreateDateColumn({ type: 'timestamp', nullable: false })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp', nullable: false })
  updatedAt: Date;

  @DeleteDateColumn({ type: 'timestamp', nullable: true })
  deletedAt: Date;
}
