import { Exclude } from 'class-transformer';
import { Event, Status } from 'src/events/entities/event.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class EventPayment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;

  @Column()
  eventTitle: string;

  @Column()
  status: Status;

  @Column('decimal')
  totalPayment: number;

  @Column()
  proofOfPayment: string;

  @Column()
  eventId: string;

  @Exclude()
  @CreateDateColumn({ type: 'timestamp', nullable: false })
  createdAt: Date;

  @Exclude()
  @UpdateDateColumn({ type: 'timestamp', nullable: false })
  updatedAt: Date;

  @Exclude()
  @DeleteDateColumn({ type: 'timestamp', nullable: true })
  deletedAt: Date;
}
