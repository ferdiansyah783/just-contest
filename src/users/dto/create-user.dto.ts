import { IsEmail, IsNotEmpty, IsOptional, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(3)
  username: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsOptional()
  about?: string;

  @IsOptional()
  address?: string;

  @IsOptional()
  instagram?: string;

  @IsOptional()
  photoProfile?: string;

  @IsOptional()
  backgroundProfile?: string;

  @IsOptional()
  accountNumber?: string;

  role: [];
}
