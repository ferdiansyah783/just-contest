import { UserIsUserGuard } from 'src/auth/guards/userIsUser.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import {
  Controller,
  Get,
  Body,
  Put,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
  UseGuards,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
  NotFoundException,
  Res,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { Roles } from 'src/users/decorators/role.decorator';
import { Role } from 'src/users/entities/role.enum';

@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('export-excel')
  async exportExcel() {
    const user = await this.usersService.exportExcel();

    return user;
  }

  @Get('download-excel')
  async getData(@Res() res) {
    return await res.download(
      `./uploads/export/user/${await this.exportExcel()}`,
      'user-data.xlsx',
    );
  }

  @Get()
  async findAll() {
    const [data, count] = await this.usersService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('user/:id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.usersService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('delete/:id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.usersService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('update-username/:id')
  async updateUsername(@Param('id') id: string, @Body() body: UpdateUserDto) {
    return {
      data: await this.usersService.updateUsername(id, body),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('update-email/:id')
  async updateEmail(@Param('id') id: string, @Body() body: UpdateUserDto) {
    return {
      data: await this.usersService.updateEmail(id, body),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('update-password/:id')
  async updatePassword(@Param('id') id: string, @Body() body: UpdateUserDto) {
    return {
      data: await this.usersService.updatePassword(id, body),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, UserIsUserGuard)
  @Put('update-profile/:id')
  async updateProfile(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return {
      data: await this.usersService.updateProfile(id, updateUserDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('upgrade-contestan/:id')
  async updateContestanRole(@Param('id', ParseUUIDPipe) id: string) {
    await this.usersService.updateContestanRole(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('upgrade-contestor/:id')
  async updateContestorRole(@Param('id', ParseUUIDPipe) id: string) {
    await this.usersService.updateContestorRole(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('role/:role')
  async findUserByRole(@Param('role') name: string) {
    const [data, count] = await this.usersService.findByRole(name);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('search-user')
  async search(@Query('username') username: string) {
    const [data, count] = await this.usersService.search(username);
    if (data.length === 0) throw new NotFoundException('Data Not Found');

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
