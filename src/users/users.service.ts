import { Role } from './entities/role.entity';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { EntityNotFoundError, Repository, Like } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { encodePassword } from 'src/auth/guards/bcrypt';
import { generateExcel } from 'src/users/helper/excel_exporter';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}

  async findUser() {
    return await this.roleRepository.find({
      where: { id: process.env.ROLE_USER },
    });
  }

  async findContestan() {
    return await this.roleRepository.find({
      where: { id: process.env.ROLE_CONTESTAN },
    });
  }

  async findContestor() {
    return await this.roleRepository.find({
      where: { id: process.env.ROLE_CONTESTOR },
    });
  }

  async create(createUserDto: CreateUserDto) {
    const userRole = await this.findUser();

    const user = new User();
    const password = encodePassword(createUserDto.password);
    user.username = createUserDto.username;
    user.email = createUserDto.email;
    user.password = password;
    user.roles = userRole;

    return await this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.findAndCount({
      relations: {
        roles: true,
      },
    });
  }

  async findOne(id: string) {
    try {
      return await this.usersRepository.findOneOrFail({
        relations: {
          roles: true,
        },
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findUsername(username: string) {
    return this.usersRepository.find({ where: { username } });
  }

  async findEmail(email: string) {
    return this.usersRepository.find({ where: { email } });
  }

  async findOneEmail(email: string) {
    return await this.usersRepository.findOne({ where: { email } });
  }

  async resetPassword(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.findOne(id);

      const user = new User();
      const password = encodePassword(updateUserDto.password);

      user.id = id;
      user.password = password;

      await this.usersRepository.save(user);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async remove(id: string) {
    try {
      await this.findOne(id);
      await this.usersRepository.delete(id);
    } catch (e) {
      throw e;
    }
  }

  async updateProfile(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.findOne(id);

      const user = new User();

      user.id = id;
      user.photoProfile = updateUserDto.photoProfile;
      user.backgroundProfile = updateUserDto.backgroundProfile;
      user.about = updateUserDto.about;
      user.address = updateUserDto.address;
      user.instagram = updateUserDto.instagram;
      user.accountNumber = updateUserDto.accountNumber;

      await this.usersRepository.save(user);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async updateUsername(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.findOne(id);

      const user = new User();
      user.id = id;
      user.username = updateUserDto.username;

      await this.usersRepository.save(user);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async updatePassword(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.findOne(id);

      const user = new User();
      const password = encodePassword(updateUserDto.password);
      user.id = id;
      user.password = password;

      await this.usersRepository.save(user);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async updateEmail(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.findOne(id);

      const user = new User();
      user.id = id;
      user.email = updateUserDto.email;

      await this.usersRepository.save(user);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async updateContestanRole(id: string) {
    try {
      await this.findOne(id);

      const contestanRole = await this.findContestan();

      const user = new User();
      user.id = id;
      user.roles = contestanRole;

      return await this.usersRepository.save(user);
    } catch (e) {
      throw e;
    }
  }

  async updateContestorRole(id: string) {
    try {
      await this.findOne(id);

      const contestorRole = await this.findContestor();

      const user = new User();
      user.id = id;
      user.roles = contestorRole;

      return await this.usersRepository.save(user);
    } catch (e) {
      throw e;
    }
  }

  async findByRole(name: string) {
    try {
      return await this.usersRepository.findAndCount({
        where: {
          roles: {
            name,
          },
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async search(username: string) {
    return await this.usersRepository.findAndCountBy({
      username: Like(`%${username}%`),
    });
  }

  async exportExcel() {
    const user = await this.usersRepository.find({
      relations: {
        roles: true,
      },
    });

    const p = [];
    user.map((data) =>
      p.push({
        username: data.username,
        email: data.email,
        createdAt: data.createdAt.toString(),
        role: data.roles[0].name,
      }),
    );

    return await generateExcel(p, 'Hit-Log-Api');
  }
}
