import { ReportContestan } from './../../contestans/entities/report-contestan.entity';
import { Contestan } from './../../contestans/entities/contestan.entity';
import { Event } from './../../events/entities/event.entity';
import { Profile } from 'src/profiles/entities/profile.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  CreateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  BeforeUpdate,
} from 'typeorm';
import { Role } from './role.entity';
import { Exclude } from 'class-transformer';
import { Like } from 'src/profiles/entities/like.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  username: string;

  @Column({ unique: true })
  email: string;

  @Exclude()
  @Column()
  password: string;

  @Column({
    default: 'profile/fec867d2-3653-452a-9c27-8daee61757b7_20220810125108.jpg',
  })
  photoProfile: string;

  @Column({
    default:
      'background-profile/0192fd24-23e9-4089-ad3f-b31d92eed405_20220811113105.jpg',
  })
  backgroundProfile: string;

  @Column({
    type: 'text',
    default: '',
  })
  about: string;

  @Column({ default: '' })
  address: string;

  @Column({ default: '' })
  instagram: string;

  @Column({ default: '' })
  accountNumber: string;

  @Exclude()
  @Column({ default: true })
  isActive: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @Exclude()
  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @Exclude()
  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Profile, (profile) => profile.user)
  profiles: Profile[];

  @OneToMany(() => Event, (event) => event.user, { cascade: true })
  events: Event[];

  @OneToMany(() => Contestan, (contestan) => contestan.user, { cascade: true })
  contestans: Contestan[];

  @OneToMany(() => Like, (like) => like.user)
  likes: Like[];

  @OneToMany(() => ReportContestan, (reportContestan) => reportContestan.user)
  reportsContestan: ReportContestan[];
}
