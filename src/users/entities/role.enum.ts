export enum Role {
  Admin = 'admin',
  Contestor = 'contestor',
  Contestan = 'contestan',
  User = 'user',
}
