import { FilesService } from './files.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import {
  editFileName,
  editFileNameBackground,
  editFileNameContestan,
  editFileNameContestanPayment,
  editFileNameEvent,
  editFileNameEventPayment,
  editFileNamePost,
  editFileNameProfile,
  editFileNameSertificat,
} from './helper/file-handler';
import * as fs from 'fs';

@Controller('files')
export class FilesController {
  constructor(private fileService: FilesService) {}

  @Get(':group/:img')
  seeUploadedFile(@Param('group') group, @Param('img') image, @Res() res) {
    try {
      const folder = `uploads/${group}`;
      const file = __dirname + `/../../${folder}/${image}`;
      if (fs.existsSync(file)) {
        return res.sendFile(image, {
          root: `./${folder}`,
        });
      } else {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'File not found',
          },
          HttpStatus.NOT_FOUND,
        );
      }
    } catch (e) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: 'File not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
    }),
  )
  async upload(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('post')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNamePost,
      }),
    }),
  )
  async uploadProfile(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('event')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameEvent,
      }),
    }),
  )
  async uploadEvent(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('photo-profile')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameProfile,
      }),
    }),
  )
  async uploadPhotoProfile(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('background-profile')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameBackground,
      }),
    }),
  )
  async uploadPhotoBackground(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('contestan')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameContestan,
      }),
    }),
  )
  async uploadContestan(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('event-payment')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameEventPayment,
      }),
    }),
  )
  async uploadEventPayment(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('contestan-payment')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameContestanPayment,
      }),
    }),
  )
  async uploadContestanPayment(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
      path: file.filename.slice(18),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('sertificate')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileNameSertificat,
      }),
    }),
  )
  async uploadSertifikat(@UploadedFile() file: Express.Multer.File) {
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
      data: await this.fileService.renameUploadFile(file.filename),
      path: file.filename.slice(18),
    };
  }
}
