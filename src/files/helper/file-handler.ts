import * as path from 'path';
import * as uuid from 'uuid';
import * as fs from 'fs';
import * as moment from 'moment';
import { HttpException, HttpStatus } from '@nestjs/common';

export const editFileName = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `${name}_${moment().format('YYYYMMDDHHmmss')}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNamePost = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/post`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `post/${name}_${moment().format('YYYYMMDDHHmmss')}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameEvent = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/event`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `event/${name}_${moment().format('YYYYMMDDHHmmss')}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameContestan = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/contestan`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `contestan/${name}_${moment().format('YYYYMMDDHHmmss')}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameEventPayment = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/event-payment`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `event-payment/${name}_${moment().format(
        'YYYYMMDDHHmmss',
      )}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameContestanPayment = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/contestan-payment`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `contestan-payment/${name}_${moment().format(
        'YYYYMMDDHHmmss',
      )}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameSertificat = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/sertificate`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `sertificate/${name}_${moment().format('YYYYMMDDHHmmss')}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameBackground = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();

  const dir = `./uploads/background-profile`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(
      null,
      `background-profile/${name}_${moment().format(
        'YYYYMMDDHHmmss',
      )}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};

export const editFileNameProfile = (req, file, callback) => {
  const fileExtName = path.extname(file.originalname);

  const dir = `./uploads/profile`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const d = new Date();

  if (
    ['.png', '.jpg', '.jpeg', '.JPEG', '.JPG', '.PNG', '.pdf', '.PDF'].includes(
      fileExtName,
    )
  ) {
    callback(null, `profile/profile_${req.user.username}${fileExtName}`);
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};
