import { User } from 'src/users/entities/user.entity';
import { UsersService } from './../../users/users.service';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'SUPER_SECRET',
    });
  }

  async validate(payload: any) {
    const user = await this.userRepository.findOne({
      relations: {
        roles: true,
      },
      where: {
        id: payload.sub,
      },
    });

    if (!user) {
      throw new HttpException(
        {
          statusCode: HttpStatus.UNAUTHORIZED,
          message: 'UNAUTHORIZED',
          data: 'Token is Invalid',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    return {
      id: user.id,
      username: user.username,
      email: user.email,
      role: user.roles,
    };
  }
}
