import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { User } from '../../users/entities/user.entity';

@Injectable()
export class UserIsUserGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const params = request.params;
    const user: User = request.user;

    if (user.id === params.id) return true;

    return false;
  }
}
