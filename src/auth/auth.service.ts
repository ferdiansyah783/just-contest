import { UpdateUserDto } from './../users/dto/update-user.dto';
import { UsersService } from './../users/users.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { comparePassword } from 'src/auth/guards/bcrypt';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MailService } from 'src/mail/mail.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private mailService: MailService,
  ) {}

  async signup(createUserDto: CreateUserDto) {
    const checkUsername = await this.userService.findUsername(
      createUserDto.username,
    );
    const checkEmail = await this.userService.findEmail(createUserDto.email);

    if (checkUsername.length) {
      throw new BadRequestException('Username has been used');
    } else if (checkEmail.length) {
      throw new BadRequestException('E-mail Already Registered');
    }

    return await this.userService.create(createUserDto);
  }

  async validateUser(username: string, pass: string) {
    const [user] = await this.userService.findUsername(username);

    if (user) {
      const matched = comparePassword(pass, user.password);
      if (matched) {
        return user;
      }
      throw new BadRequestException('Password false');
    }
    throw new BadRequestException('Username false');
  }

  async login(user: any) {
    const existUser = await this.userService.findOne(user.id);
    const payload = {
      username: existUser.username,
      sub: existUser.id,
      role: existUser.roles,
    };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async sendResetToken(email: string) {
    const existUser = await this.userService.findOneEmail(email);
    if (!existUser) throw new BadRequestException('RESET.USER_NOT_FOUND');

    return this.mailService.sendResetToken(email, existUser.id);
  }

  async resetPassword(id: string, updateUserDto: UpdateUserDto) {
    return await this.userService.resetPassword(id, updateUserDto);
  }
}
