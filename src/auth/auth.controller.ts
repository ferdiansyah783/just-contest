import { UpdateUserDto } from './../users/dto/update-user.dto';
import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signup')
  async signup(@Body() createUserDto: CreateUserDto) {
    return {
      data: await this.authService.signup(createUserDto),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Get('send-reset-link')
  async sendResetToken(@Query('email') email: string) {
    return await this.authService.sendResetToken(email);
  }

  @Put('reset-password/:id')
  async resetPassword(@Param('id') id: string, @Body() body: UpdateUserDto) {
    return await this.authService.resetPassword(id, body);
  }
}
