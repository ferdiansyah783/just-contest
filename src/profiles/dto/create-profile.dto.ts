import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { User } from 'src/users/entities/user.entity';
import { Like } from '../entities/like.entity';

export class CreateProfileDto {
  @IsOptional()
  title?: string;

  @IsOptional()
  description?: string;

  @IsNotEmpty()
  @IsString()
  linkPath: string;

  like: Like[];

  user: User;
}
