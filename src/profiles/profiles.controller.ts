import { UserIsUserGuard } from '../auth/guards/userIsUser.guard';
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Request,
  HttpStatus,
  ParseUUIDPipe,
  Put,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@Controller('posts')
@UseInterceptors(ClassSerializerInterceptor)
export class ProfilesController {
  constructor(private readonly profilesService: ProfilesService) {}

  @UseGuards(JwtAuthGuard)
  @Post('create')
  async createPost(@Body() createProfileDto: CreateProfileDto, @Request() req) {
    const user = req.user;
    return {
      data: await this.profilesService.create(user, createProfileDto),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  @Get()
  async findAll() {
    const [data, count] = await this.profilesService.findAll();

    return {
      data,
      count,
      stausCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('post/:id')
  async findPost(@Param('id', ParseIntPipe) id: number) {
    return {
      data: await this.profilesService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('user/:id')
  async findPostByUserId(@Param('id', ParseUUIDPipe) id: string) {
    const [data, count] = await this.profilesService.findByUserId(id);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, UserIsUserGuard)
  @Put('update/:id')
  async updatePost(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProfileDto: UpdateProfileDto,
  ) {
    return {
      data: await this.profilesService.update(id, updateProfileDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard)
  @Delete('delete/:id')
  async deletePost(@Param('id', ParseIntPipe) id: number) {
    await this.profilesService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('like')
  async updateLike(@Request() req, @Query('post', ParseIntPipe) post: number) {
    return await this.profilesService.updateLike(req.user, post);
  }

  @Get('sum-like/:post')
  async sumLike(@Param('post', ParseIntPipe) post: number) {
    const [data, count] = await this.profilesService.sumLike(post);

    return {
      data,
      count,
    };
  }
}
