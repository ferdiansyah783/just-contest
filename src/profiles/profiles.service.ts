import { User } from '../users/entities/user.entity';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository } from 'typeorm';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile } from './entities/profile.entity';
import { Like } from './entities/like.entity';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class ProfilesService {
  constructor(
    @InjectRepository(Profile) private profileRepository: Repository<Profile>,
    @InjectRepository(Like) private likeRepository: Repository<Like>,
  ) {}

  async create(user: User, createProfileDto: CreateProfileDto) {
    const profile = new Profile();
    profile.linkPath = createProfileDto.linkPath;
    profile.title = createProfileDto.title;
    profile.description = createProfileDto.description;
    profile.user = user;

    console.log(profile.like);

    return await this.profileRepository.save(profile);
  }

  async findAll() {
    return await this.profileRepository.findAndCount({
      order: {
        id: 'ASC',
      },
      relations: {
        user: true,
        like: true,
      },
    });
  }

  async findOne(id: number) {
    try {
      return await this.profileRepository.findOneOrFail({
        where: {
          id,
        },
        relations: {
          user: true,
          like: true,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findByUserId(id: string) {
    try {
      return await this.profileRepository.findAndCount({
        relations: {
          user: true,
        },
        where: {
          user: {
            id,
          },
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: number, updateProfileDto: UpdateProfileDto) {
    try {
      await this.findOne(id);
      await this.profileRepository.update(id, updateProfileDto);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async remove(id: number) {
    try {
      await this.findOne(id);
      await this.profileRepository.delete(id);
    } catch (e) {
      throw e;
    }
  }

  async findOneLike(id: number) {
    try {
      return await this.likeRepository.findOne({
        relations: { user: true },
        where: { posts: { id } },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async removeLike(id: string) {
    try {
      return await this.likeRepository.delete(id);
    } catch (e) {
      throw e;
    }
  }

  async findLike(id: string) {
    return await this.likeRepository.find({
      relations: {
        user: true,
        posts: true,
      },
      where: {
        user: {
          id,
        },
      },
    });
  }

  async updateLike(user: User, post: number) {
    const existsPost = await this.findOne(post);
    const existLike = await this.findOneLike(post);

    console.log(existLike);

    if (existLike) {
      const el = await this.findOneLike(post);
      await this.removeLike(el.id);

      return {
        statusCode: HttpStatus.OK,
        message: 'success deleted',
      };
    }

    const like = new Like();

    like.like = +1;
    like.user = user;
    like.posts = existsPost;

    await this.likeRepository.save(like);

    return existsPost;
  }

  async sumLike(id: number) {
    console.log(id);
    return await this.likeRepository.findAndCount({
      relations: {
        user: true,
      },
      where: { posts: { id } },
    });
  }
}
