import { profitContest } from 'src/events/helper/contest-ppn';
import { User } from 'src/users/entities/user.entity';
import { Event, Status } from './entities/event.entity';
import {
  HttpException,
  HttpStatus,
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EntityNotFoundError, Like, Repository } from 'typeorm';
import { eventGenerateExcel } from './helper/excel_exporter';

@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event) private eventRepository: Repository<Event>,
  ) {}

  async create(user: User, createEventDto: CreateEventDto) {
    try {
      const existTitle = await this.eventRepository.find({
        where: {
          title: createEventDto.title,
        },
      });

      if (existTitle.length)
        throw new BadRequestException('Title already used');

      const event = new Event();

      event.title = createEventDto.title;
      event.description = createEventDto.description;
      event.thumbnail = createEventDto.thumbnail;
      event.competitionPrice = createEventDto.competitionPrice;
      event.ppn = profitContest(createEventDto.competitionPrice);
      event.deadline = createEventDto.deadline;
      event.status = createEventDto.status;
      event.user = user;

      return await this.eventRepository.save(event);
    } catch (e) {
      throw e;
    }
  }

  async findAll() {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
    });
  }

  async findAndCount() {
    return await this.eventRepository.findAndCount({
      relations: { contestans: true, user: true },
      take: 10,
      order: {
        competitionPrice: 'ASC',
      },
      where: {
        status: Status.READY,
      },
    });
  }

  async findPendingEvent() {
    return this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        status: Status.PENDING,
      },
    });
  }

  async findReadyEvent() {
    return this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        status: Status.READY,
      },
    });
  }

  async findFinishEvent() {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        status: Status.FINISHED,
      },
    });
  }

  async findReadyEventByUser(user: string) {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
        contestans: true,
      },
      where: {
        contestans: {
          user: {
            id: user,
          },
        },
        status: Status.READY,
      },
    });
  }

  async findFinishEventByUser(user: string) {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
        contestans: true,
      },
      where: {
        contestans: {
          user: {
            id: user,
          },
        },
        status: Status.FINISHED,
      },
    });
  }

  async findOne(id: string) {
    try {
      return await this.eventRepository.findOneOrFail({
        relations: {
          user: true,
        },
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findContestByContestan(id: string) {
    try {
      return await this.eventRepository.find({
        relations: {
          contestans: true,
          user: true,
        },
        where: {
          contestans: {
            user: {
              id,
            },
          },
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findTitle(title: string) {
    try {
      return await this.eventRepository.findOne({
        relations: { user: true },
        where: { title },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findContestor(username: string) {
    return await this.eventRepository.find({
      relations: {
        user: true,
      },
      where: {
        user: {
          username,
        },
      },
    });
  }

  async findByUserId(id: string) {
    try {
      return await this.eventRepository.findAndCount({
        relations: {
          user: true,
        },
        where: {
          user: {
            id,
          },
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findAllWaitingApprove() {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        status: Status.PENDING,
        statusPayment: true,
      },
    });
  }

  async findWaitingApprove(id: string) {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        user: {
          id,
        },
        status: Status.PENDING,
        statusPayment: true,
      },
    });
  }

  async findWaitingPayment(id: string) {
    return await this.eventRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        user: {
          id,
        },
        status: Status.PENDING,
        statusPayment: false,
      },
    });
  }

  async update(id: string, updateEventDto: UpdateEventDto) {
    try {
      await this.findOne(id);
      await this.eventRepository.update(id, updateEventDto);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async findPaymentStatus() {
    try {
      return await this.eventRepository.findAndCount({
        relations: ['user'],
        where: { statusPayment: true },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async updateReadyStatus(id: string) {
    try {
      await this.findOne(id);

      const event = new Event();

      event.id = id;
      event.statusPayment = true;
      event.status = Status.READY;

      await this.eventRepository.save(event);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async updateFinishStatus(id: string) {
    try {
      await this.findOne(id);

      const event = new Event();

      event.id = id;
      event.statusPayment = true;
      event.status = Status.FINISHED;

      await this.eventRepository.save(event);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async updatePaymentStatus(id: string) {
    try {
      await this.findOne(id);
    } catch (e) {
      throw new NotFoundException('Data Not Found');
    }

    const event = new Event();

    event.id = id;
    event.statusPayment = true;

    await this.eventRepository.save(event);

    return await this.findOne(id);
  }

  async updateStatusEvent(id: string) {
    try {
      await this.findOne(id);

      const event = new Event();
      event.id = id;
      event.isActive = false;

      await this.eventRepository.save(event);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async remove(id: string) {
    try {
      await this.eventRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.eventRepository.delete(id);
  }

  async search(title: string) {
    return await this.eventRepository.findAndCountBy({
      title: Like(`%${title}%`),
    });
  }

  async exportExcel() {
    const event = await this.eventRepository.find({
      relations: {
        user: true,
      },
    });

    const temp = [];
    event.map((data) =>
      temp.push({
        title: data.title,
        contestor: data.user.username,
        competitionPrice: data.competitionPrice,
        status: data.status,
        createdAt: data.createdAt.getTime().toString(),
      }),
    );

    return await eventGenerateExcel(temp, 'Hit-Log-Api');
  }
}
