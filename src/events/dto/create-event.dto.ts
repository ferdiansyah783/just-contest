import { IsNotEmpty, IsNumber, IsString, MinLength } from 'class-validator';
import { User } from 'src/users/entities/user.entity';
import { Status } from '../entities/event.entity';

export class CreateEventDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  title: string;

  @IsNotEmpty()
  @IsNumber()
  competitionPrice: number;

  ppn: number;

  @IsNotEmpty()
  @IsString()
  thumbnail: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  deadline: Date;

  isActive: boolean;

  status: Status;

  user: User;
}
