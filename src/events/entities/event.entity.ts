import { Contestan } from './../../contestans/entities/contestan.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';

export enum Status {
  PENDING = 'pending',
  READY = 'ready',
  FINISHED = 'finished',
}

@Entity()
export class Event {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  title: string;

  @Column('decimal')
  competitionPrice: number;

  @Column('decimal')
  ppn: number;

  @Column()
  thumbnail: string;

  @Column()
  deadline: Date;

  @Column('text')
  description: string;

  @Column({ default: true })
  isActive: boolean;

  @Column({ type: 'enum', enum: Status, default: Status.PENDING })
  status: Status;

  @Column({ default: false })
  statusPayment: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @Exclude()
  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @Exclude()
  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToOne(() => User, (user) => user.events, { onDelete: 'CASCADE' })
  user: User;

  @OneToMany(() => Contestan, (contestan) => contestan.event, { cascade: true })
  contestans: Contestan[];
}
