export function contestPpn(price: number) {
  const ppn = 8 / 100;
  const profit = price * ppn;
  return price - profit;
}

export function profitContest(price: number) {
  const ppn = 8 / 100;
  return price * ppn;
}
