import { excelInserter } from './excel_inserter';
import * as moment from 'moment';
import * as path from 'path';
import { HttpException, HttpStatus } from '@nestjs/common';

export async function eventGenerateExcel(data, type) {
  try {
    const filename = `${moment().format('YYYYMMDDHHMM')}-${type}.xlsx`;
    const filepath = path.join(
      __dirname + '../../../../uploads/export/event',
      filename,
    );
    console.log(data, 'ini datanya');
    await DataToExcel(filepath, data, type);
    return filename;
  } catch (e) {
    console.log(e, 'generateExcel');
    throw new HttpException(
      {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}

const hitLogApiData = [
  {
    title: 'title',
    dataIndex: 'title',
    width: 20,
  },
  {
    title: 'contestor',
    dataIndex: 'contestor',
    width: 20,
  },
  {
    title: 'competitionPrice',
    dataIndex: 'competitionPrice',
    width: 20,
  },
  {
    title: 'status',
    dataIndex: 'status',
    width: 20,
  },
  {
    title: 'createdAt',
    dataIndex: 'createdAt',
    width: 20,
  },
];

async function setDataHitLogApi(dataHitLogApi) {
  try {
    const dataFiltered = [];

    await dataHitLogApi.forEach((element) => {
      const filterElement = [];

      filterElement['title'] = element?.title;
      filterElement['contestor'] = element?.contestor;
      filterElement['competitionPrice'] = element?.competitionPrice;
      filterElement['status'] = element?.status;
      filterElement['createdAt'] = element?.createdAt;

      dataFiltered.push(filterElement);
    });

    return dataFiltered;
  } catch (e) {
    console.log(e, 'setDataHitLogAPI');
    throw new HttpException(
      {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}

export async function DataToExcel(filepath, data, type) {
  try {
    if (type === 'Hit-Log-Api') {
      const dataFiltered = await setDataHitLogApi(data);

      await excelInserter({
        filename: filepath,
        sheets: [
          {
            name: 'Data Hit Log Api',
            startRowFrom: 1,
            headers: hitLogApiData,
            data: dataFiltered,
          },
        ],
      });
    } else if (type === 'XXX 1') {
      //kode yang sama tapi beda set header dan data nya
    } else if (type === 'Customer-Register') {
      //kode yang sama tapi beda set header dan data nya
    } else if (type === 'Summary-Services') {
      //kode yang sama tapi beda set header dan data nya
    } else if (type === 'Top-Rate') {
      //kode yang sama tapi beda set header dan data nya
    }
  } catch (e) {
    console.log(e, 'dataToExcel');
    throw new HttpException(
      {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}
