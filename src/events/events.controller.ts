import { RolesGuard } from 'src/auth/guards/roles.guard';
import { JwtAuthGuard } from './../auth/guards/jwt-auth.guard';
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Request,
  HttpStatus,
  UseGuards,
  ParseUUIDPipe,
  Put,
  Query,
  UseInterceptors,
  ClassSerializerInterceptor,
  NotFoundException,
  Res,
} from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { Roles } from 'src/users/decorators/role.decorator';
import { Role } from 'src/users/entities/role.enum';
import { UserIsUserGuard } from 'src/auth/guards/userIsUser.guard';

@Controller('events')
@UseInterceptors(ClassSerializerInterceptor)
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Post('create')
  async createEvent(@Body() createEventDto: CreateEventDto, @Request() req) {
    const user = req.user;
    return {
      data: await this.eventsService.create(user, createEventDto),
      statusCode: HttpStatus.CREATED,
      message: 'Event Creation Successful, Please Complete Payment',
    };
  }

  @Get()
  async findAll() {
    const [data, count] = await this.eventsService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('pending')
  async findPendingEvent() {
    const [data, count] = await this.eventsService.findPendingEvent();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('finished')
  async findFinishedEvent() {
    const [data, count] = await this.eventsService.findFinishEvent();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('ready')
  async findReadyEvent() {
    const [data, count] = await this.eventsService.findReadyEvent();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event/:id')
  async findEvent(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.eventsService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-limit')
  async findAndCount() {
    const [data, count] = await this.eventsService.findAndCount();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-contestan/:id')
  async findContestansContest(@Param('id') id: string) {
    return {
      data: await this.eventsService.findContestByContestan(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-ready/:user')
  async findReadyEventByUser(@Param('user') user: string) {
    const [data, count] = await this.eventsService.findReadyEventByUser(user);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-finish/:user')
  async findFinishEventByUser(@Param('user') user: string) {
    const [data, count] = await this.eventsService.findFinishEventByUser(user);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('title')
  async findTitle(@Query('title') event: string) {
    return await this.eventsService.findTitle(event);
  }

  @Get('event-contestor/:id')
  async findByUserId(@Param('id', ParseUUIDPipe) id: string) {
    const [data, count] = await this.eventsService.findByUserId(id);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('payment-status')
  async findEventPaymentStatus() {
    const [data, count] = await this.eventsService.findPaymentStatus();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('payment-waiting-approve')
  async findAllWaitingAproof() {
    const [data, count] = await this.eventsService.findAllWaitingApprove();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('payment-waiting-approve/:id')
  async findWaitingAproof(@Param('id') id: string) {
    const [data, count] = await this.eventsService.findWaitingApprove(id);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('payment-waiting-payment/:id')
  async findWaitingPayment(@Param('id') id: string) {
    const [data, count] = await this.eventsService.findWaitingPayment(id);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, UserIsUserGuard)
  @Put('update/:id')
  async updateEvent(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateEventDto: UpdateEventDto,
  ) {
    return {
      data: await this.eventsService.update(id, updateEventDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Put('update-status-ready/:id')
  async updateReadyStatus(@Param('id') id: string) {
    return {
      data: await this.eventsService.updateReadyStatus(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Put('update-status-finish/:id')
  async updateFinishStatus(@Param('id') id: string) {
    return {
      data: await this.eventsService.updateFinishStatus(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Put('update-event-status/:id')
  async updateEventStatus(@Param('id') id: string) {
    return {
      data: await this.eventsService.updateStatusEvent(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Put('update-payment-status/:id')
  async updatePaymentStatus(@Param('id') id: string) {
    return {
      data: await this.eventsService.updatePaymentStatus(id),
      statusCode: HttpStatus.OK,
      message: 'successs',
    };
  }

  @Delete('delete/:id')
  async deleteEvent(@Param('id', ParseUUIDPipe) id: string) {
    await this.eventsService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('search')
  async search(@Query('title') title: string) {
    const [data, count] = await this.eventsService.search(title);

    if (data.length === 0) {
      throw new NotFoundException('Data Not Found');
    }

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('download-excel')
  async exportExcel(@Res() res) {
    return await res.download(
      `./uploads/export/event/${await this.eventsService.exportExcel()}`,
      'event-data.xlsx',
    );
  }
}
