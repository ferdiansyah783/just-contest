import { Contestan } from './contestan.entity';
import { User } from 'src/users/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ReportContestan {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  option: string;

  @Column({ default: '' })
  message: string;

  @ManyToOne(() => User, (user) => user.reportsContestan)
  user: User;

  @ManyToOne(() => Contestan, (contestan) => contestan.reportsContestan)
  contestan: Contestan;
}
