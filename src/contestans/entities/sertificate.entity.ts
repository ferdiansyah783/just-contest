import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Contestan } from './contestan.entity';

@Entity()
export class Sertificate {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  linkPath: string;

  @ManyToOne(() => Contestan, (contestan) => contestan.sertificates)
  contestan: Contestan;
}
