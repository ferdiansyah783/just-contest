import { ReportContestan } from './report-contestan.entity';
import { Event } from './../../events/entities/event.entity';
import { User } from './../../users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { Sertificate } from './sertificate.entity';

export enum ContestanStatus {
  WINNER = 'winner',
  CONTESTAN = 'contestan',
}

@Entity()
export class Contestan {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  linkPath: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ default: 0, type: 'decimal' })
  rating: number;

  @Column({
    type: 'enum',
    enum: ContestanStatus,
    default: ContestanStatus.CONTESTAN,
  })
  status: ContestanStatus;

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @Exclude()
  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @Exclude()
  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToOne(() => User, (user) => user.contestans, { onDelete: 'CASCADE' })
  user: User;

  @ManyToOne(() => Event, (event) => event.contestans, { onDelete: 'CASCADE' })
  event: Event;

  @OneToMany(
    () => ReportContestan,
    (reportContestan) => reportContestan.contestan,
  )
  reportsContestan: ReportContestan[];

  @OneToMany(() => Sertificate, (sertificate) => sertificate.contestan)
  sertificates: Sertificate[];
}
