import { ReportContestan } from './entities/report-contestan.entity';
import { UsersModule } from 'src/users/users.module';
import { EventsModule } from './../events/events.module';
import { Contestan } from './entities/contestan.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { ContestansService } from './contestans.service';
import { ContestansController } from './contestans.controller';
import { Sertificate } from './entities/sertificate.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Contestan, ReportContestan, Sertificate]),
    EventsModule,
    UsersModule,
  ],
  providers: [ContestansService],
  controllers: [ContestansController],
  exports: [ContestansService],
})
export class ContestansModule {}
