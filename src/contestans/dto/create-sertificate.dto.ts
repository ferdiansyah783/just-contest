import { IsNotEmpty, IsString } from 'class-validator';
import { Contestan } from '../entities/contestan.entity';

export class CreateSertificateDto {
  @IsNotEmpty()
  @IsString()
  linkPath: string;

  contestan: Contestan;
}
