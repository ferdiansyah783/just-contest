import { IsOptional } from 'class-validator';

export class CreateReportDto {
  @IsOptional()
  option?: string;

  @IsOptional()
  message?: string;
}
