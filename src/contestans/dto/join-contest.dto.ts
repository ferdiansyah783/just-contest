import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Event } from 'src/events/entities/event.entity';
import { User } from 'src/users/entities/user.entity';
import { ContestanStatus } from '../entities/contestan.entity';

export class CreateContestanDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  @IsString()
  linkPath: string;

  @IsOptional()
  rating?: number;

  user: User;

  event: Event[];

  status: ContestanStatus;
}
