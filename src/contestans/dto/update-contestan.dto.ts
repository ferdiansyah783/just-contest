import { CreateContestanDto } from './join-contest.dto';
import { PartialType } from '@nestjs/mapped-types';

export class UpdateContestanDto extends PartialType(CreateContestanDto) {}
