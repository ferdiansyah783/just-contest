import { UserIsUserGuard } from '../auth/guards/userIsUser.guard';
import { UpdateContestanDto } from './dto/update-contestan.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CreateContestanDto } from './dto/join-contest.dto';
import { ContestansService } from './contestans.service';
import {
  Body,
  Controller,
  Post,
  Query,
  Request,
  HttpStatus,
  UseGuards,
  Get,
  Param,
  Put,
  UseInterceptors,
  Delete,
  ClassSerializerInterceptor,
  Res,
} from '@nestjs/common';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Roles } from 'src/users/decorators/role.decorator';
import { Role } from 'src/users/entities/role.enum';
import { Event } from 'src/events/entities/event.entity';
import { CreateReportDto } from './dto/create-report.dto';
import { CreateSertificateDto } from './dto/create-sertificate.dto';

@Controller('contestans')
@UseInterceptors(ClassSerializerInterceptor)
export class ContestansController {
  constructor(private contestanService: ContestansService) {}

  @UseGuards(JwtAuthGuard)
  @Post('create/:event')
  async createContestan(
    @Request() req,
    @Param('event') event: string,
    @Body() createContestanDto: CreateContestanDto,
  ) {
    console.log(event);
    return {
      data: await this.contestanService.create(
        req.user,
        event,
        createContestanDto,
      ),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  @Get()
  async findAll() {
    const [data, count] = await this.contestanService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('contestan/:id')
  async findContestan(@Param('id') id: string) {
    return {
      data: await this.contestanService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get('winner-contestan')
  async findWinner() {
    const [data, count] = await this.contestanService.findWinner();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('winner-contestan/event/:event')
  async findOneWinner(@Param('event') event: string) {
    return {
      data: await this.contestanService.findOneWinnerByEventId(event),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('winner-contestan/:id')
  async findOneWinnerById(@Param('id') id: string) {
    return {
      data: await this.contestanService.findWinnerById(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('contestan/:user/:event')
  async findByUserAndEventId(
    @Param('user') user: string,
    @Param('event') event: string,
  ) {
    const [data, count] = await this.contestanService.findByUserAndEventId(
      user,
      event,
    );

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Put('update-rating/:id')
  async updateContestan(
    @Param('id') id: string,
    @Body() updateContestanDto: UpdateContestanDto,
  ) {
    return {
      data: await this.contestanService.updateRating(id, updateContestanDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('delete/:id')
  async removeContestan(@Param('id') id: string) {
    await this.contestanService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('search')
  async search(@Query('username') username: string) {
    const [data, count] = await this.contestanService.search(username);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-submit')
  async findByEvent(@Query('id') id: string) {
    const [data, count] = await this.contestanService.findByEvent(id);

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('event-submit/:event/contestan/:id')
  async findContestanByEventId(
    @Param('event') event: string,
    @Param('id') id: string,
  ) {
    return {
      data: await this.contestanService.findByEventId(event, id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Contestor)
  @Put('upgrade-winner/:event/:id')
  async updateStatus(@Param('event') event: string, @Param('id') id: string) {
    return {
      data: await this.contestanService.updateStatus(event, id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('export-pdf-sertifikat')
  async generatePdf() {
    return await this.contestanService.generatePdf();
  }

  @Get('download-pdf-sertifikat')
  async downloadPdf(@Res() res) {
    return res.download(`uploads/pdf/test.pdf`);
  }

  @Get('download-detail-excel')
  async exportExcel(@Res() res) {
    return await res.download(
      `./uploads/export/contestan/${await this.contestanService.exportExcel()}`,
      'contestan-data.xlsx',
    );
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Post('create-sertificate/:contestan')
  async createSertificate(
    @Param('contestan') contestan: string,
    @Body() createSertificateDto: CreateSertificateDto,
  ) {
    return {
      data: await this.contestanService.createUploadSertificate(
        contestan,
        createSertificateDto,
      ),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  @Get('certifikate/:contestan')
  async findCertifikate(@Param('contestan') contestan: string) {
    return {
      data: await this.contestanService.findCertifikate(contestan),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Report
  @UseGuards(JwtAuthGuard)
  @Post('create-report/:contestanId')
  async createReport(
    @Request() req,
    @Param('contestanId') contestanId: string,
    @Body() body: CreateReportDto,
  ) {
    console.log(req.user);
    return await this.contestanService.createReport(
      req.user,
      contestanId,
      body,
    );
  }

  @Get('report')
  async findReport() {
    const [data, count] = await this.contestanService.findReport();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('download-sertificate/:contestan')
  async test(@Param('contestan') contestan: string, @Res() res) {
    const sertificate = await this.contestanService.downloadSertificate(
      contestan,
    );
    return res.download(sertificate);
  }
}
