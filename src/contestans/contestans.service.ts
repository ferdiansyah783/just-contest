import { ReportContestan } from './entities/report-contestan.entity';
import { UpdateContestanDto } from './dto/update-contestan.dto';
import { EventsService } from './../events/events.service';
import { CreateContestanDto } from './dto/join-contest.dto';
import { Contestan, ContestanStatus } from './entities/contestan.entity';
import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository, Like } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import * as puppeteer from 'puppeteer';
import * as fs from 'fs';
import { contestanGenerateExcel } from './helper/excel_exporter';
import { Event } from 'src/events/entities/event.entity';
import { CreateReportDto } from './dto/create-report.dto';
import { Sertificate } from './entities/sertificate.entity';
import { CreateSertificateDto } from './dto/create-sertificate.dto';

@Injectable()
export class ContestansService {
  constructor(
    @InjectRepository(Contestan)
    private contestanRepository: Repository<Contestan>,
    @InjectRepository(ReportContestan)
    private reportContestanRepository: Repository<ReportContestan>,
    private eventService: EventsService,
    @InjectRepository(Sertificate)
    private sertificateRepository: Repository<Sertificate>,
  ) {}

  async create(
    user: User,
    event: string,
    createContestanDto: CreateContestanDto,
  ) {
    try {
      await this.eventService.findOne(event);
    } catch (e) {
      throw new NotFoundException('Event Not Found');
    }

    const existEvent = await this.eventService.findOne(event);

    const contestan = new Contestan();

    contestan.title = createContestanDto.title;
    contestan.description = createContestanDto.description;
    contestan.linkPath = createContestanDto.linkPath;
    contestan.status = createContestanDto.status;
    contestan.user = user;
    contestan.event = existEvent;

    return await this.contestanRepository.save(contestan);
  }

  async findAll() {
    return await this.contestanRepository.findAndCount({
      relations: {
        user: true,
        event: true,
      },
    });
  }

  async findOne(id: string) {
    try {
      return await this.contestanRepository.findOneOrFail({
        relations: {
          event: true,
          user: true,
        },
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findWinner() {
    return await this.contestanRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        status: ContestanStatus.WINNER,
      },
    });
  }

  async findOneWinnerByEventId(event: string) {
    try {
      const existEvent = await this.eventService.findOne(event);

      return await this.contestanRepository.findOne({
        relations: {
          event: true,
          user: true,
        },
        where: {
          event: {
            id: existEvent.id,
          },
          status: ContestanStatus.WINNER,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findWinnerById(id: string) {
    try {
      return await this.contestanRepository.findOneOrFail({
        relations: {
          user: true,
          event: true,
        },
        where: {
          id,
          status: ContestanStatus.WINNER,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async remove(id: string) {
    try {
      await this.findOne(id);
      await this.contestanRepository.delete(id);
    } catch (e) {
      throw e;
    }
  }

  async search(username: string) {
    return await this.contestanRepository.findAndCountBy({
      user: {
        username: Like(`%${username}%`),
      },
    });
  }

  async findByEvent(id: string) {
    return await this.contestanRepository.findAndCount({
      relations: {
        user: true,
      },
      where: {
        event: {
          id,
        },
      },
    });
  }

  async findByUserAndEventId(userId: string, eventId: string) {
    try {
      return await this.contestanRepository.findAndCount({
        relations: { user: true, event: true },
        where: { user: { id: userId }, event: { id: eventId } },
      });
    } catch (e) {
      throw new NotFoundException('Data Not Found');
    }
  }

  async findByEventId(event: string, id: string) {
    try {
      return await this.contestanRepository.findOneOrFail({
        relations: {
          event: true,
        },
        where: {
          event: {
            id: event,
          },
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async updateStatus(event: string, id: string) {
    try {
      await this.contestanRepository.findOneOrFail({
        relations: {
          event: true,
        },
        where: {
          event: {
            id: event,
          },
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const contestan = new Contestan();

    contestan.id = id;
    contestan.status = ContestanStatus.WINNER;

    await this.contestanRepository.save(contestan);

    return await this.contestanRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async updateRating(id: string, updateContestanDto: UpdateContestanDto) {
    try {
      await this.findOne(id);

      const contestan = new Contestan();

      contestan.id = id;
      contestan.rating = updateContestanDto.rating;

      await this.contestanRepository.save(contestan);

      return await this.findOne(id);
    } catch (e) {
      throw e;
    }
  }

  async createUploadSertificate(
    id: string,
    createSertificateDto: CreateSertificateDto,
  ) {
    try {
      await this.findOne(id);

      const contestanDb = await this.findOne(id);

      const sertificate = new Sertificate();
      sertificate.linkPath = createSertificateDto.linkPath;
      sertificate.contestan = contestanDb;

      return await this.sertificateRepository.save(sertificate);
    } catch (e) {
      throw e;
    }
  }

  async generatePdf() {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    const html = fs.readFileSync(`html/index.html`, 'utf-8');
    await page.setContent(html, {
      waitUntil: 'domcontentloaded',
    });
    const pdfBuffer = await page.pdf({ format: 'A4' });
    await page.pdf({
      format: 'A4',
      path: 'uploads/pdf/test.pdf',
      landscape: true,
    });

    await browser.close();
  }

  async findOneSertificateByContestan(id: string) {
    try {
      return await this.sertificateRepository.findOne({
        relations: {
          contestan: true,
        },
        where: {
          contestan: {
            id,
          },
        },
      });
    } catch (e) {
      throw new NotFoundException('Data Not Found');
    }
  }

  async downloadSertificate(id: string) {
    try {
      await this.findOneSertificateByContestan(id);

      const sertificate = await this.findOneSertificateByContestan(id);
      return process.cwd() + `/uploads/${sertificate.linkPath}`;
    } catch (e) {
      throw new BadRequestException('Sertificate Not Found');
    }
  }

  async findCertifikate(id: string) {
    return await this.sertificateRepository.find({
      relations: {
        contestan: true,
      },
      where: {
        contestan: {
          id,
        },
      },
    });
  }

  async exportExcel() {
    const contestans = await this.contestanRepository.find({
      relations: {
        event: true,
        user: true,
      },
      where: {
        status: ContestanStatus.WINNER,
      },
    });

    const temp = [];
    contestans.map((data) =>
      temp.push({
        username: data.user.username,
        event: data.event.title,
        submited: data.title,
        status: data.status,
        createdAt: data.createdAt.toString(),
      }),
    );

    return await contestanGenerateExcel(temp, 'Hit-Log-Api');
  }

  // Report
  async createReport(req, contestanId: string, body: CreateReportDto) {
    try {
      await this.findOne(contestanId);
      const contestan = await this.findOne(contestanId);

      const report = new ReportContestan();

      report.option = body.option;
      report.message = body.message;
      report.user = req;
      report.contestan = contestan;

      await this.reportContestanRepository.save(report);

      return 'success';
    } catch (e) {
      throw e;
    }
  }

  async findReport() {
    return await this.reportContestanRepository.findAndCount({
      relations: { user: true, contestan: true },
    });
  }
}
