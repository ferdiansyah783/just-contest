import { UsersService } from 'src/users/users.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class MailService {
  constructor(private userService: UsersService) {}

  async sendMail(id: string) {
    const existUser = await this.userService.findOne(id);

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL,
        pass: 'qkxurtvceozpgqjq',
      },
    });

    const mailOption = {
      from: 'justcontest97@gmail.com',
      to: existUser.email,
      subject: 'Test Email',
      text: 'hello I am from Just Contest',
    };

    return transporter.sendMail(mailOption, (err, info) => {
      if (err) throw err;
      console.log('Email send : ', info.response);
    });
  }

  async sendResetToken(email: string, id: string) {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    const mailOption = {
      from: process.env.EMAIL,
      to: email,
      subject: 'Reset Token',
      text: 'link',
      html: `
        <div style="text-align: center; font-size: 25px;">
        <h1>Reset Password</h1>
        <p>This is your link to reset your password</p>
        <a href="http://localhost:3000/auth/reset/${id}" style="text-decoration: none;">
          Click Here
        </a>
        </div>
      `,
    };

    return transporter.sendMail(mailOption, (err, info) => {
      if (err) throw err;
      console.log('Email send: ', info.response);
    });
  }

  async sendImg(email: string, img: string) {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    const mailOption = {
      from: process.env.EMAIL,
      to: email,
      subject: 'Proof Of Payment',
      attachments: [
        {
          filename: img,
          path: process.cwd() + `/uploads/contestan-payment/${img}`,
          cid: 'logo',
        },
      ],
      html: `<h1 style='text-align: center;'>Congratulation</h1>
            <h2 style='text-align: center;'>This is proof of payment because you have won the contest at JustContest</h2>
            <h2 style='text-align: center;'>Thanks for the participation</h2>
           <p><img src = 'cid:logo'></img></p>
           <h3>Please download your sertificate at the current page contest<h3/>`,
    };

    return transporter.sendMail(mailOption, (err, info) => {
      if (err) throw err;
      console.log('Email send: ', info.response);
    });
  }
}
